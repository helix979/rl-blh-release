"""
The MIT License

Copyright (c) 2017 Jinkyu Koo

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to 
deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, 
merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom 
the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice 
shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')

import sys
import re
import os
import matplotlib.pyplot as plt
import pickle
import numpy as np

nAct = int(sys.argv[1])
maxUse = float(sys.argv[2])
batCapacity = float(sys.argv[3])

subdir  = sys.argv[4]
filename = sys.argv[5]
os.chdir(os.getcwd())
os.chdir(subdir)

try:
    f = open(filename, "r")
    lines = f.readlines()
    f.close()
except:
    sys.exit("%s does not exist.\n" % (filename))

trace = []    
for line in lines:
    s = re.split('\t|\n', line)
    s = filter(None, s)
    trace.append(s)
t = zip(*trace)
Len = len(t[0])
x = range(Len)

major_ticks = np.arange(0, len(x)+1, 240)
minor_ticks = np.arange(0, len(x)+1, 5)


plt.figure()
plt.subplot(311)
use = [float(i) for i in t[0]]
plt.plot(x, use)
plt.ylabel('$x_n$',fontsize=20)
plt.xlim(0, Len)
plt.ylim(0, maxUse)
plt.gca().axes.get_xaxis().set_ticks(major_ticks)
plt.gca().set_xticklabels([])
plt.grid()



plt.subplot(312)
draw = [float(i) for i in t[1]]
plt.plot(x, draw)
plt.ylabel('$y_n$',fontsize=20)
plt.xlim(0, Len)
plt.ylim(0, maxUse)
plt.gca().axes.get_xaxis().set_ticks(major_ticks)
plt.gca().set_xticklabels([])
plt.grid()

plt.subplot(313)
bat = [float(i) for i in t[2]]
f1, = plt.plot(x, bat)
plt.ylabel('$b_n$',fontsize=20)
plt.xlabel('measurement interval $n$',fontsize=20)
plt.xlim(0, Len)
plt.ylim(0, batCapacity)
plt.gca().axes.get_xaxis().set_ticks(major_ticks)
plt.grid()


figname = filename[:-4] + ".pdf"
plt.savefig(figname)

#os.system("evince " + figname)

