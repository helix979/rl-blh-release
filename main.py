"""
The MIT License

Copyright (c) 2017 Jinkyu Koo

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to 
deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, 
merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom 
the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice 
shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import rlblh
import use
import random
import sys
import data_gen
    
    
logInterval = 10
logDelay = 10

epsilon = 0.1
numDay = 1000000000
numIntervalDay = 1440
learningInt = 15
maxUse = 0.08
batCapacity = 5
priceProfile = [0.0704]*(numIntervalDay-420) + [0.2109]*(420)

   

rlblh = rlblh.RLBLH(batCapacity, numIntervalDay, learningInt, maxUse, priceProfile)
use =  use.UseProcess(0, maxUse, numIntervalDay)

mode = 'training'

if (len(sys.argv) >= 2):
    w_file = sys.argv[1] # feed the name of a w*.dat file
    rlblh.loadW(w_file)

if (len(sys.argv) == 3):
    mode = sys.argv[2] # feed 'run'
    
if (mode == 'run'):
    numDay = 1
    rlblh.initBattery()
    epsilon = 0


nStep = 30
fname = "synthetic_data_model.dat"
dgen = data_gen.DataGen(maxUse, nStep, numIntervalDay)
dgen.loadModel(fname)
day = 1
ave_saving_ratio = 0
for d in xrange(numDay):
    cost = 0
    saving = 0
    loss = 0

    useTrace = []
    drawTrace = []
    for n in range(numIntervalDay):

        u = dgen.genVal(n)
        c, s, draw, l = rlblh.run(n, u, epsilon)
        cost += c
        saving += s
        loss += l
        useTrace.append(u)
        drawTrace.append(draw)
    rlblh.saveSavingTrace(d, 60, 1)
    rlblh.saveLossTrace(d, 60, 1, "loss_trace.txt")    

    if d>=logDelay:
        day += 1
        ave_saving_ratio += saving/cost*100.0

    if (d % logInterval == 0):    
        print "day = %d, cost = %.4f, saving = %.4f, ratio = %.4f, ave_saving_ratio=%.4f" % (d, cost, saving, saving/cost*100.0, ave_saving_ratio/float(day))
        rlblh.saveTrace(d)
        rlblh.saveW(d)


    

    
        
