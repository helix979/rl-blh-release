"""
The MIT License

Copyright (c) 2017 Jinkyu Koo

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to 
deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, 
merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom 
the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice 
shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import random
import data_proc
import pickle
import os.path


dataSubdir = "homeC-power"

##################################################
class DataGen:
    def __init__(self, _max, nStep, numInterval):
        self.max = _max
        self.nStep = nStep
        self.resolution = _max/float(nStep)
        self.numInterval = numInterval
        self.intProfile = [[0 for i in xrange(nStep)] for j in xrange(numInterval)]
        self.cnt = [0 for i in xrange(numInterval)]
                
    def collectVal(self, intIndex, val):
        stepIndex = int(val/float(self.resolution))
        # assert stepIndex < self.nStep
        if stepIndex >= self.nStep:        
            print "stepIndex >= self.nStep"
        else:
            self.cnt[intIndex] += 1
            self.intProfile[intIndex][stepIndex] += 1

    def genVal(self, intIndex):
        cnt = self.cnt[intIndex]
        u = random.random()
        n = 0
        # print repr(self.intProfile[intIndex]), cnt
        for s in xrange(self.nStep):
            n += self.intProfile[intIndex][s]/float(cnt)
            if (u <= n):
                return s*self.resolution #+ self.resolution/2.0
                
    def makeModel(self, filename):
        dp = data_proc.DataProc(self.numInterval, dataSubdir)
        numDay = dp.getNumDay()

        for d in xrange(numDay):    
            for n in range(self.numInterval):
                u = dp.getVal(d, n)
                self.collectVal(n, u)
       
        fdat = open(filename, 'wb')    
        pickle.dump([self.intProfile, self.cnt], fdat)
        fdat.close()         

    def loadModel(self, filename):
        fdat = open(filename, 'rb')
        self.intProfile, self.cnt = pickle.load(fdat)
        fdat.close()                 
##################################################

if __name__ == "__main__":
    _max = 0.08
    nStep = 30
    numInterval = 1440
    fname = "synthetic_data_model.dat"
    d = DataGen(_max, nStep, numInterval)
    if not os.path.isfile(fname):
        d.makeModel(fname)
        
    d.loadModel(fname)
    for day in xrange(1):    
        for n in xrange(5):    
            print d.genVal(n)
