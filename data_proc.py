"""
The MIT License

Copyright (c) 2017 Jinkyu Koo

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to 
deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, 
merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom 
the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice 
shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import glob
import os
import sys
import calendar
import re

SEC_A_DAY = 86400

##################################################
class DataProc:
    def __init__(self, numIntervalDay, subdir):
        self.numIntervalDay = numIntervalDay
        self.secPerUnit = int(SEC_A_DAY/numIntervalDay)
        curdir = os.getcwd()
        self.subdir = os.path.join(curdir, subdir)
        os.chdir(self.subdir)
        flist = glob.glob("*.csv")
        os.chdir(curdir)
        self.flist_sorted = self.sortList(flist)
        self.nDay = len(self.flist_sorted)     
        self.use_seq = None
        

    def getNumDay(self):
        return self.nDay
        
        
    def sortList(self, flist):
        fbook = {}
        convert2num = {v: k for k,v in enumerate(calendar.month_abbr)}
        expr = r'(\d+)-([A-Z-a-z]+)-(\d+)'
        for f in flist:
            m = re.search(expr, f, flags=0)
            if m:
                year = m.group(1)
                mon = str(convert2num[m.group(2)]).zfill(2)
                day = m.group(3).zfill(2)
                val = int(year + mon + day)
                fbook[f] = val
        flist_sorted = sorted(fbook, key=fbook.get)
        return flist_sorted


    def readFile(self, file):
        data = []
        file = os.path.join(self.subdir, file)
        with open(file, 'r') as fd:
            for line in fd.readlines():
                s = filter(None, re.split(',|, |\n', line))
                # print repr(s)
                data.append(s)
        return data
    

    def makeDataDay(self, dayIndex):
        data = self.readFile(self.flist_sorted[dayIndex])
        use_seq = []
        print self.flist_sorted[dayIndex]
        cnt = 0
        val = 0
        for d in data:
            val += float(d[1])
            cnt += 1
            if (cnt == self.secPerUnit):
                # change from Ws to kWh
                val = val/1000.0/3600.0
                use_seq.append(val)
                val = 0
                cnt = 1
        
        last = len(use_seq)-1
        while last < self.numIntervalDay:
            use_seq.insert(0, use_seq[0])
            last += 1

        if last >= self.numIntervalDay:
            use_seq = use_seq[:self.numIntervalDay]
            
        # assert len(use_seq) == self.numIntervalDay
        # print repr(use_seq)
        return use_seq
       
    def getVal(self, dayIndex, intIndex):
        if intIndex == 0:
            self.use_seq = self.makeDataDay(dayIndex)
        return self.use_seq[intIndex]
            


##################################################        

