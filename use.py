"""
The MIT License

Copyright (c) 2017 Jinkyu Koo

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to 
deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, 
merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom 
the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice 
shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import random

##################################################
class UseProcess:
    def __init__(self, _min, _max, _numInterval, _intProfile = None):
        self.min = _min
        self.max = _max
        self.numInterval = _numInterval
        if (_intProfile != None):
            assert _numInterval == len(_intProfile)        
            self.intProfile = _intProfile
        else:
            # 0: low activity
            # 1: high activity
            self.intProfile = [0]*_numInterval 
            for i in range(_numInterval/2, _numInterval):
                self.intProfile[i] = 1
                
    def getVal(self, _intIndex):
        # Will use the Irwin-Hall distribution
        ret = 0
        r = lambda : random.random() * self.max/10.0
        if (self.intProfile[_intIndex] == 0):
            for i in xrange(3):
                ret += r()
        if (self.intProfile[_intIndex] == 1):
            for i in xrange(6):
                ret += r()
        return ret                
##################################################



