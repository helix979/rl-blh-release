"""
The MIT License

Copyright (c) 2017 Jinkyu Koo

Permission is hereby granted, free of charge, 
to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to 
deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, 
merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom 
the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice 
shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, 
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR 
ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, 
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import math
import sys
import random
import draw_constant
import os
import datetime
import pickle
import numpy as np
import data_gen

subdir = 'results'
alpha = 0.05
nFeat = 6
nAct = 8 
normalActSet = range(nAct)
obsTh = 100
nQuanStep = 30
learningTh = 10

class RLBLH:
    def __init__(self, batCapacity, numIntervalDay, learningInt, maxUse, priceProfile):
        self.batLevel = 0
        self.batCapacity = batCapacity
        self.numIntervalDay = numIntervalDay
        self.maxUse = maxUse
        self.maxDraw = maxUse
        self.priceProfile = priceProfile
        self.action = None
        self.savingDay = 0
        self.costDay = 0
        self.lossDay = 0
        self.reward = 0
        self.curBatLevel = 0
        self.learningInt = learningInt
        assert (numIntervalDay % learningInt) == 0
        self.nLearning = numIntervalDay/learningInt
        self.useSeq = []
        self.batLevelSeq = []
        
        self.w = [[0 for i in xrange(nFeat)] for j in xrange(nAct)]
        
        self.qCnt = [0 for j in xrange(nAct)]
        self.globalW = [random.random() for i in xrange(nFeat)]

        self.draw = draw_constant.DrawProcess(0, self.maxDraw, nAct)
        
        self.trace = [None]*numIntervalDay
        
        now = datetime.datetime.now()
        postfix = "_%04d%02d%02d_%02d%02d%02d" % (now.year, now.month, now.day, now.hour, now.minute, now.second)
        self.subdir = subdir + postfix
        
        self.featL = [[] for j in xrange(nAct)]
        self.obsL = [[] for j in xrange(nAct)]
        self.obsCnt = [0 for j in xrange(nAct)]

        self.featL1 = [[] for j in xrange(nAct)]
        self.featL2= [[] for j in xrange(nAct)]
        self.rL= [[] for j in xrange(nAct)]        
        self.obsCnt2 = [0 for j in xrange(nAct)]
        
        self.day = 0
        self.dgen = data_gen.DataGen(maxUse, nQuanStep, numIntervalDay)

        self.dataGen = True
        self.leastSquare = False
        self.reuse = True
        self.totalUse = 0
        
        # For comparison
        self.use_prev = 0


        
    def possibleActions(self, b):
        if (b <= self.maxDraw*self.learningInt):
            return [nAct-1]
        elif (b >= self.batCapacity - self.maxDraw*self.learningInt):
            return [0]
        else:
            return normalActSet
		
    def initBattery(self, val = 0):
        self.batLevel = val


    def feature(self, n, b):
        N = (n)/float(self.nLearning)
        B = b/float(self.batCapacity)
        feat = [1, N, B, N*B, pow(N,2), pow(B,2)]
        return feat

    def q(self, n, b, a, tryThisW = None):
        feat = self.feature(n, b)
        s = 0
        w = self.w[a]
        if tryThisW:
            w = tryThisW
        for i in xrange(nFeat):
            s += w[i] * feat[i]
        return s

    def updateQCnt(self, n, a):        
        self.qCnt[a] += 1

    def determineOptAct(self, n, b):
        actions = self.possibleActions(b)
        q = lambda a: self.q(n, b, a)
        return max(actions, key=q)

    def updateW(self, r, n, b, a, b2):
        qVal = 0
        if (n < self.nLearning-1):
            a2 = self.determineOptAct(n+1, b2)
            qVal = self.q(n+1, b2, a2)
        observe = r + qVal
        feat = self.feature(n, b)
        newW = [None]*nFeat
        for i in xrange(nFeat):
            newW[i] = self.w[a][i] - alpha*(self.q(n, b, a) - observe)*feat[i]
        self.w[a] = newW[:]

        return pow(self.q(n, b, a) - observe, 2)

    def ls(self, r, n, b, a, b2):
        qVal = 0
        if (n < self.nLearning-1):
            a2 = self.determineOptAct(n+1, b2)
            qVal = self.q(n+1, b2, a2)
        observe = r + qVal
        feat = self.feature(n, b)
        newW = [None]*nFeat
        
        self.featL[a].append(feat)
        self.obsL[a].append([observe])        
        self.obsCnt[a] += 1

        if (self.obsCnt[a] == obsTh):
            A = np.array(self.featL[a])
            B = np.array(self.obsL[a])
            R = np.linalg.inv(np.matmul(A.transpose(), A))
            W = np.matmul(np.matmul(R, A.transpose()), B)
            self.featL[a] = []
            self.obsL[a] = []
            self.obsCnt[a] = 0
            self.w[a] = list(W.reshape(nFeat))


    def lspi(self, r, n, b, a, b2):
        feat1 = self.feature(n, b)
        feat2 = self.feature(n+1, b2)
        
        self.featL1[a].append(feat1)
        self.featL2[a].append(feat2)        
        self.rL[a].append([r])        
        self.obsCnt2[a] += 1

        if (self.obsCnt2[a] == obsTh):
            PI1 = np.array(self.featL1[a])
            PI2 = np.array(self.featL2[a])
            R = np.array(self.rL[a])            
            print repr(PI1-PI2)
            A = np.matmul(PI1.transpose(), PI1-PI2)
            B = np.matmul(PI1.transpose(), R)

            Inv = np.matmul(A.transpose(), np.linalg.inv(np.matmul(A, A.transpose())))
            W = np.matmul(Inv,B)
            
            self.featL1[a] = []
            self.featL2[a] = []
            self.rL[a] = []
            self.obsCnt2[a] = 0
            self.w[a] = list(W.reshape(nFeat))
            
      
    def saveObs(self, a, feat, obs, w):
        self.featL[a].append(feat)
        self.obsL[a].append([obs])        
        self.obsCnt[a] += 1

        if (self.obsCnt[a] == obsTh):
            A = np.array(self.featL[a])
            B = np.array(self.obsL[a])
            R = np.linalg.inv(np.matmul(A.transpose(), A))
            W = np.matmul(np.matmul(R, A.transpose()), B)
            self.featL[a] = []
            self.obsL[a] = []
            self.obsCnt[a] = 0
            return list(W.reshape(nFeat))
        return w        
        
        
    def policy(self, n, b, epsilon):
        action = None

        if (random.random() <= epsilon):
            action = random.choice(self.possibleActions(b))
        else:
            action = self.determineOptAct(n, b)
        return action

        
    def run_core(self, intIndex, use, trace, epsilon):
        curBatLevel = self.batLevel
        
        if (intIndex % self.learningInt == 0):
            action = self.policy(intIndex/self.learningInt, curBatLevel, epsilon)
            self.action = action
            self.updateQCnt(intIndex/self.learningInt, action)
            self.curBatLevel = curBatLevel
        else:
            action = self.action
        
        draw = self.draw.getVal(action)
        diff = draw - use
        self.batLevel += diff

        assert self.batLevel >= 0
        assert self.batLevel <= self.batCapacity
        
        nextBatLevel = self.batLevel
        cost = use * self.priceProfile[intIndex]
        saving = self.priceProfile[intIndex] * (use - draw)
        self.reward += saving

        loss = 0
        if (intIndex % self.learningInt == (self.learningInt-1)):
            loss = self.updateW(self.reward, int(intIndex/self.learningInt), self.curBatLevel, action, nextBatLevel)
            if (self.leastSquare == True):
                self.ls(self.reward, int(intIndex/self.learningInt), self.curBatLevel, action, nextBatLevel)                
            # self.lspi(self.reward, int(intIndex/self.learningInt), self.curBatLevel, action, nextBatLevel)                
            self.reward = 0
        
        if (trace == 'on'):
            if (intIndex == 0):
                self.savingDay = 0        
                self.costDay = 0
                self.lossDay = 0
                # self.totalUse = 0
            # self.totalUse += use
            self.savingDay += saving
            self.costDay += cost
            self.lossDay += loss
            self.trace[intIndex] = [use, draw, curBatLevel, action,\
                                     self.q(int(intIndex/self.learningInt), self.curBatLevel, action),\
                                     self.qCnt[action]]
            # if (intIndex == self.numIntervalDay-1):
                # print self.totalUse
        return cost, saving, draw, loss

        
    def run(self, intIndex, use, epsilon = 0.1):

        self.dgen.collectVal(intIndex, use)
        if (intIndex == 0):
            self.day += 1 
            global alpha
            alpha /= math.sqrt(self.day)
            if (alpha < 0.01):
                alpha = 0.01
            epsilon /= math.sqrt(self.day)
                
            
        if (self.dataGen == True):
            if (intIndex == self.numIntervalDay-1) and (self.day % 10 == 0) and (self.day <= 100050):
                for d in xrange(500):
                    for i in xrange(self.numIntervalDay):
                        self.run_core(i, self.dgen.genVal(i), 'off', epsilon)
        
        if (intIndex == 0):
            self.useSeq = []
            self.batLevelSeq = []
        self.useSeq.append(use)
        self.batLevelSeq.append(self.batLevel)
        
        ret = self.run_core(intIndex, use, 'on', epsilon)

        if (self.reuse == True) and (self.day <= 100020):
            if (intIndex == self.numIntervalDay-1):
                self.batLevel = self.batLevelSeq[0]    
                for i in xrange(100):
                    for i in xrange(0, self.numIntervalDay):
                          self.run_core(i, self.useSeq[i], 'off', epsilon)


        
        return ret 

        
    def saveTrace(self, dayIndex):
        if not os.path.exists(self.subdir):
            os.makedirs(self.subdir)

        rmDay = dayIndex - 100000
        rmFile = os.path.join(self.subdir, "trace" + str(rmDay) + ".txt")
        if os.path.isfile(rmFile):
            os.remove(rmFile)

        rmFile = os.path.join(self.subdir, "trace" + str(rmDay) + ".pdf")
        if os.path.isfile(rmFile):
            os.remove(rmFile)
            
        filename = os.path.join(self.subdir, "trace" + str(dayIndex) + ".txt")
        fd = open(filename, 'w')
        length = len(self.trace[0])
        for n in xrange(self.numIntervalDay):
            for l in range(length):
                fd.write(str(self.trace[n][l]) + '\t')
            fd.write('\n')
        fd.close()
        param = str(nAct) + ' ' + str(self.maxUse) + ' ' + str(self.batCapacity) + ' ' 
        os.system('python plot_trace.py ' + param + self.subdir + " trace" + str(dayIndex) + ".txt")
        
    def saveW(self, dayIndex):
        if not os.path.exists(self.subdir):
            os.makedirs(self.subdir)

        rmDay = dayIndex - 100000
        rmFile = os.path.join(self.subdir, "w" + str(rmDay) + ".dat")
        if os.path.isfile(rmFile):
            os.remove(rmFile)

        filename = os.path.join(self.subdir, "w" + str(dayIndex) + ".dat")
        fdat = open(filename, 'wb')    
        pickle.dump([self.w, self.qCnt], fdat)
        fdat.close()
        

    def plotQ(self, dayIndex):
        if not os.path.exists(self.subdir):
            os.makedirs(self.subdir)

        rmDay = dayIndex - 100000
        rmFile = os.path.join(self.subdir, "q" + str(rmDay) + ".pdf")
        if os.path.isfile(rmFile):
            os.remove(rmFile)
        filename = os.path.join(self.subdir, "q" + str(dayIndex) + ".pdf")
        wfile = os.path.join(self.subdir, "w" + str(dayIndex) + ".dat")
        param = wfile + ' ' + str(self.batCapacity) + ' ' + str(self.numIntervalDay) + ' ' + str(nAct) + ' ' 
        os.system('python plot_q.py ' + param + filename)
        
        
            
    def loadW(self, filename):
        self.w, self.qCnt = pickle.load(open(filename, "rb" ))
   

    def saveSavingTrace(self, dayIndex, lastDayIndex, resolution):
        if not os.path.exists(self.subdir):
            os.makedirs(self.subdir)
            
        name = "saving_trace.txt"
        if dayIndex < lastDayIndex:
            filename = os.path.join(self.subdir, name)
            with open(filename, "a") as fd:
                fd.write("%d\t%f\n" % (dayIndex, self.savingDay/float(self.costDay)*100))
        if dayIndex == lastDayIndex:
            os.system('python plot_saving_trace.py ' + str(lastDayIndex) + ' ' + str(resolution) + ' ' + self.subdir + ' ' + name)
      
    def saveLossTrace(self, dayIndex, lastDayIndex, resolution, name):
          
        if dayIndex < lastDayIndex:
            with open(name, "a") as fd:
                fd.write("%d\t%f\n" % (dayIndex, self.lossDay))
        # if dayIndex == lastDayIndex:
            # os.system('python plot_loss_trace.py ' + str(lastDayIndex) + ' ' + str(resolution) + ' ' + self.subdir + ' ' + name)    
